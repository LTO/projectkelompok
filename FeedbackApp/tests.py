from django.test import TestCase, Client
from django.urls import resolve
from .views import index, create
from django.http import HttpRequest
# Create your tests here.
class Mengetes(TestCase):

	def test_mengecek_ada_IsiFeedback_jalan(self):
		c = Client()
		response = c.get("/IsiFeedback/")
		content = response.content.decode('utf8')
		self.assertEqual(response.status_code, 200)

	def test_mengecek_ada_tulisan_Saran_di_IsiFeedback(self):
		request = HttpRequest()
		c = Client()
		response = c.get('/IsiFeedback/')
		content = response.content.decode('utf8')
		self.assertIn('Ada Saran Untuk Kami?', content)

	def test_apakah_ada_button(self):
		c = Client()
		response = c.get('/IsiFeedback/')
		content = response.content.decode('utf8')
		self.assertIn('<button', content)

	def test_apakah_ada_table(self):
		c = Client()
		response = c.get('/IsiFeedback/HasilFeedback')
		content = response.content.decode('utf8')
		self.assertIn('<table', content)

	def test_apa_IsiFeedback_menggunakan_template_InputSaran(self):
		c = Client()
		response = c.get('/IsiFeedback/')
		self.assertTemplateUsed(response, 'InputSaran.html')

	def test_apa_HasilFeedback_menggunakan_template_OutputSaran(self):
		c = Client()
		response = c.get('/IsiFeedback/HasilFeedback')
		self.assertTemplateUsed(response, 'OutputSaran.html')

	def test_apa_IsiFeedback_ada_fungsi_create(self):
		found = resolve('/IsiFeedback/')
		self.assertEqual(found.func, create)

	def test_apa_IsiFeedback_ada_fungsi_index(self):
		found = resolve('/IsiFeedback/HasilFeedback')
		self.assertEqual(found.func, index)