from django import forms
from .models import ModelFeedback
from django.forms import ModelForm, TextInput

class FormFeedback(forms.ModelForm):
	class Meta(object):
		model = ModelFeedback
		fields = [
			'Nama',
			'Pesan',
		]
		widgets = {
			'Nama': TextInput(),
			'Pesan': TextInput(),
		}
