
from django.shortcuts import render, redirect
from .forms import FormFeedback
from .models import ModelFeedback

# Create your views here.
def index(request):
	modelFeedback = ModelFeedback.objects.all()
	context = {
		'hasilFeedback' : modelFeedback,
	}

	return render(request, 'OutputSaran.html', context)

def create(request):
	formFeedback = FormFeedback(request.POST or None)	# request.POST buat validasi

	# Masukin dari form ke database
	if request.method == 'POST':
		if formFeedback.is_valid():	# Kalau input valid
			formFeedback.save()
			
			return redirect('/IsiFeedback/HasilFeedback')	# Abis ngisi form redirect ke halaman schedule

	context = {
		'formFeedback' : formFeedback,
	}

	return render(request, 'InputSaran.html', context)
