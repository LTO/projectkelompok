from django.contrib import admin
from django.conf.urls import url
from django.urls import path, re_path
from . import views

app_name = 'FeedbackApp'

urlpatterns = [
	path('HasilFeedback', views.index),
	path('', views.create, name='create'),	
	path('admin/', admin.site.urls),
]