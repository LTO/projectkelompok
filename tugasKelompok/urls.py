from django.urls import path, include
from django.contrib import admin

#from . import views

urlpatterns = [
	path('admin/', admin.site.urls),
    path('', include('tugasKelompokApp.urls')),
    path('HasilFeedback/', include('FeedbackApp.urls')),
    path('homepage/', include('tugasKelompokApp.urls')),
    #path('HasilFeedback/', include('FeedbackApp.urls')),
    path('admin/', admin.site.urls),
    path('testimony/', include('TestimonyApp.urls')),
    path('inputMakananApp/', include('inputMakananApp.urls')),
]
