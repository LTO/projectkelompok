from django.urls import path, include
from django.contrib import admin
from .views import *

app_name = 'tugasKelompokApp'

urlpatterns = [
    path('', homepage, name='homepage'),
    path('homepage/', homepage, name="homepage"),
    path('submitisibuku/', submitIsiBuku, name="submitisibuku"),
    path('IsiFeedback/', include('FeedbackApp.urls')),
    path('admin/', admin.site.urls),
    path('testimony/', include('TestimonyApp.urls')),
 	path('inputMakananApp/', include('inputMakananApp.urls')),
]

#
#from django.contrib import admin
#from django.urls import path, include
#from . import views

#urlpatterns = [
#    path('admin/', admin.site.urls),
#    path('', include('tugasKelompokApp.urls')),
#    path('HasilFeedback/', include('FeedbackApp.urls')),
#]