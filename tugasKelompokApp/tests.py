from django.test import TestCase, Client
from django.urls import resolve
from .views import homepage
from django.http import HttpRequest

# Create your tests here.
class TestShinta (TestCase) :
	def test_apakah_url_homepage_ada(self):
		response = Client().get('/homepage/')
		self.assertEqual(response.status_code, 200)


	def test_apakah_templatenya_pakai_index_html(self):
		response = Client().get('/homepage/')
		self.assertTemplateUsed(response, 'index.html')


	def test_apakah_nama_fungsi_diviewsnya_index (self) :
		found = resolve ('/homepage/')
		self.assertEqual(found.func, homepage)

	def test_apakah_ada_tulisan_calorie (self):
		request = HttpRequest()
		response = homepage (request)
		html_response = response.content.decode('utf8')
		self.assertIn ('CALORIE', html_response)

	