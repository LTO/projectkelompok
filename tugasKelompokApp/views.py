from django.shortcuts import render, redirect
from .forms import IsiBukuTamuForm
from .models import IsiBukuTamu

def index(request):
    return render(request, 'HelloWorld.html')

def homepage(request):
    listBuku = IsiBukuTamu.objects.all().values()
    return render(request, 'index.html', {'list' : listBuku})

def submitIsiBuku (request):
	if request.method == 'POST':
		form = IsiBukuTamuForm(request.POST)
		if form.is_valid():
			print ("hello")
			nama = request.POST ['nama']
			email = request.POST['email']
			tanggal = request.POST ['tanggal']
			submit = IsiBukuTamu(nama=nama, email=email, tanggal=tanggal)
			submit.save()
	return redirect('/inputMakananApp/')




