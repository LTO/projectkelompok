from django.apps import AppConfig


class TugaskelompokappConfig(AppConfig):
    name = 'tugasKelompokApp'
