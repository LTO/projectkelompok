from django.test import TestCase, Client
from django.urls import resolve
from .views import inputan
from django.http import HttpRequest

# Create your tests here.
class TestInputMakanan (TestCase):
    def tes_apakah_ada_inputan(self):
        response = Client().get('/inputan/')
        self.assertEqual(response.status_code, 200)

    def tes_apakah_ada_outputkalori(self):
        response = Client().get('/outputkalori/')
        self.assertEqual(response.status_code, 200)

    def tes_apakah_ada_horey(self):
        response = Client().get('/horey/')
        self.assertEqual(response.status_code, 200)
    
    def tes_apakah_ada_tulisan_makan_apa_aja_hari_ini(self):
        response = inputan(request).get('/inputan/')
        self.assertContains(response, 'Makan apa aja hari ini?')
        self.assertEqual(response.status_code, 200)





