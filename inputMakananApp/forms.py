from django import forms

class formMakanan(forms.Form):
    menuUtama = forms.CharField(max_length=255, widget = forms.TextInput(attrs={'style' : 'font-size: 1vw', 'class' : 'form-control', 'placeholder' : 'Menu utama', 'id' : 'menuUtama'}))
    lauk = forms.CharField(max_length=255, widget = forms.TextInput(attrs={'style' : 'font-size: 1vw','class' : 'form-control', 'placeholder' : 'Lauk', 'id' : 'lauk'}))
    sayur = forms.CharField(max_length=255, widget = forms.TextInput(attrs={'style' : 'font-size: 1vw','class' : 'form-control', 'placeholder' : 'Sayur', 'id' : 'sayur'}))
