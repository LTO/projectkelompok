from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'inputMakananApp'

urlpatterns = [
    path('', views.inputan, name='inputan'),
    path('outputkalori/', views.outputkalori, name='outputkalori'),
    path('horey/', views.horey, name='horey'),
    path('tambahMakanan/', views.tambahMakanan, name='tambahMakanan'),
]
