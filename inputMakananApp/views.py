from django.shortcuts import render, redirect
from django.views.decorators.http import require_POST
from .forms import formMakanan
from .models import listMakanan, kaloriMakanan

def inputan(request):
    list_makanan = listMakanan.objects.order_by('id')
    list_ngasal = kaloriMakanan.objects.order_by('id')
    form = formMakanan()
    context = {'list_makanan' : list_makanan, list_ngasal : 'list_ngasal', 'form' : form}
    return render(request, 'inputan.html', context)

def outputkalori(request):
	return render(request,'outputkalori.html')

def horey(request):
	return render(request,'horey.html')

@require_POST
def tambahMakanan(request):
    form = formMakanan(request.POST)

    makananBaru = listMakanan(menuUtama = request.POST['menuUtama'], lauk = request.POST['lauk'], sayur = request.POST['sayur'])
    makananBaru.save()
    kaloriNgasal = kaloriMakanan(kaloriMenuMakanan = 20)
    kaloriNgasal.save()

    return redirect('inputMakananApp:outputkalori')

