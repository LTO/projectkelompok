CALORIEKU
============

[![Pipeline](https://gitlab.com/LTO/projectkelompok/badges/master/pipeline.svg)](https://gitlab.com/LTO/projectkelompok/)
[![Coverage](https://gitlab.com/LTO/projectkelompok/badges/master/coverage.svg)](https://gitlab.com/LTO/projectkelompok/)

Anggota Kelompok __KE02__:
* Hilmi Arista - 1806147003
* Shinta Fauziah - 1806147180
* Thalia Teresa - 1806190992
* Sulthan Zahran - 1806191585

Link Heroku: [CALORIEKU](https://helloworldapp123456.herokuapp.com/)

## Penjelasan
Web ini bertujuan untuk menghitung jumlah kalori yang diterima oleh seseorang setelah mengkonsumsi sarapan, makan siang dan makan malam


## Fitur-fitur

* Menghitung kalori seusai asupan
* Saran sesuai jumlah kalori
