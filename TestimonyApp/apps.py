from django.apps import AppConfig


class TestimonyappConfig(AppConfig):
    name = 'TestimonyApp'
