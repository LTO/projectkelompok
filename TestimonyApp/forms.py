from django import forms
from .models import ModelTestimony

class FormTestimony(forms.ModelForm):
    class Meta(object):
        model = ModelTestimony
        fields = [
            'nama', 
            'email', 
            'kesan',
        ]
        