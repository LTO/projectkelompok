from django.shortcuts import render,redirect
from .forms import FormTestimony
from .models import ModelTestimony
# Create your views here.

def index(request):
    modelTestimony = ModelTestimony.objects.all()
    context = {
        'hasilTestimony' : modelTestimony
    }
#    return render(request, 'testimony.html', context)

#def show(request):
    if request.method == 'POST':
        form = FormTestimony(request.POST)
        if form.is_valid():
            nama = request.POST['nama']
            email = request.POST['email']
            kesan = request.POST['kesan']
            addTestimony = ModelTestimony(nama=nama, email=email, kesan=kesan)
            addTestimony.save()
#    return redirect('/testimony/')
    return render(request, 'testimony.html', context)