from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest

class ThaliaTest(TestCase):
    #apakah url testimony ada
    def test_app_url_is_exist(self): 
        response = Client().get('/testimony/')
        self.assertEqual(response.status_code,200)
    
    #apakah template yg dipakai testimony.html
    def test_app_using_testimony_template(self):
        response = Client().get('/testimony/')
        self.assertTemplateUsed(response, 'testimony.html')

    #apakah nama fungsi di views nya testimony
    def test_app_using_testimony_func(self):
        found = resolve('/testimony/')
        self.assertEqual(found.func, index)
    
    #apakah ketika url testimony di buka, ada tulisan testimony
    def test_landing_page_content(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn( 'Tulis Testimoni yuk!', html_response)
        
    #apakah kalau submit datanya masuk
    #def test_create_message_models(self):
    #    c = Message.objects.all().count()
    #    Message.objects.create(message='Test')
    #    cnt = Message.objects.all().count()
    #    self.assertEqual(cnt, c+1)