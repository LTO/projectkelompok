from django.db import models

class ModelTestimony(models.Model):
    nama = models.CharField(max_length=30)
    email = models.EmailField(max_length=50)
    kesan = models.CharField(max_length=150)