from django.contrib import admin
from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'TestimonyApp'

urlpatterns = [
    path('', views.index, name='index'),
]